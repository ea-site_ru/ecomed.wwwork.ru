$(document).ready(function() {
    (function initMainpageSliders() {
        // главный слайдер на главной странице
        var mainpageSlider = new Swiper('#mainpage_slider', {
            loop: true,
            autoplay: {
                delay: 3500,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            }
        });
    
        // слайдер распродаж на главной
        var mainpageSalesSlider = new Swiper('#mainpage_sale_slider', {
            slidesPerView: 4,
            spaceBetween: 16,
            autoplay: {
                delay: 3500,
                disableOnInteraction: false,
            },
            scrollbar: {
                el: '.swiper-scrollbar',
                draggable: true,
            },
            on: {
                reachEnd: function() {
                    // дошли до последнего слайда, теперь после delay мс из autoplay нужно перемотать на первый слайд
                    setTimeout(function() {
                        mainpageSalesSlider.slideTo(0, mainpageSalesSlider.params.speed);
                    }, mainpageSalesSlider.params.autoplay.delay);
                }
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                },
                992: {
                    slidesPerView: 2,
                },
                1199: {
                    slidesPerView: 3,
                },
            }
        });

        // слайдер в блоке "О компании"
        var headers = [],
            mainpage_about_slider = new Swiper('#mainpage_about_slider', {
                centeredSlides: true,
                effect: 'fade',
                autoplay: {
                    delay: 10000,
                },
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '"><span class="slide-number">' + ( (index < 8) ? '0' : '' ) + (index + 1) + '</span>' + headers[index] + '</span>';
                    },
                },
                on: {
                    init: function() {
                        var slider = this;

                        // управление автопереключением при наведении курсора
                        slider.el.addEventListener('mouseenter', function() {
                            slider.autoplay.stop();
                        });
                
                        slider.el.addEventListener('mouseleave', function() {
                            slider.autoplay.start();
                        });

                        // получаем заголовки слайдов
                        $('#mainpage_about_slider .swiper-slide[data-slide-header]:not(.swiper-slide-duplicate)').each(function() {
                            var slide_header = $(this).data('slide-header');
                            if ( headers.indexOf(slide_header) < 0 )
                                headers.push(slide_header);
                        });
                    },
                    slideChangeTransitionEnd: function() {
                        $('#mainpage_about_company_subheader').text( $('#mainpage_about_slider .swiper-slide-active').data('slide-header') );
                        $('#mainpage_about_company_text').html( $('#mainpage_about_slider .swiper-slide-active .slide__text').html() );
                    }
                }
            });

        // слайдер партнёров на главной
        var mainpagePartnersSlider = new Swiper('#mainpage_partners_slider', {
            slidesPerView: 6,
            autoplay: {
                delay: 3500,
                disableOnInteraction: false,
            },
            scrollbar: {
                el: '.swiper-scrollbar',
                draggable: true,
            },
            on: {
                reachEnd: function() {
                    // дошли до последнего слайда, теперь после delay мс из autoplay нужно перемотать на первый слайд
                    setTimeout(function() {
                        mainpagePartnersSlider.slideTo(0, mainpagePartnersSlider.params.speed);
                    }, mainpagePartnersSlider.params.autoplay.delay);
                }
            },
            breakpoints: {
                767: {
                    slidesPerView: 2,
                },
                992: {
                    slidesPerView: 4,
                },
            }
        });

        // слайдер персонала на странице контактов
        var contacts_page_employees_slider = new Swiper('#contacts_page_employees_slider', {
            slidesPerView: 4,
            spaceBetween: 17,
            navigation: {
                nextEl: '#contacts_page_employees_slider + .contacts-page-employees-slider-arrows .swiper-button-next',
                prevEl: '#contacts_page_employees_slider + .contacts-page-employees-slider-arrows .swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                },
                992: {
                    slidesPerView: 2,
                },
                1199: {
                    slidesPerView: 3,
                },
            }
        });
    })();
});