// меняем цену товара в зависимости от выбранного кол-ва
function rewriteProductPrice(input, prod_id) {
    if ( parseInt(input.val()) >= 100 && parseInt(input.val()) < 500 ) {
        cur_price = $('[data-prod-' + prod_id + '-cur-price]').data('prod-' + prod_id + '-price-over-100');
    } else if ( parseInt(input.val()) >= 500 ) {
        cur_price = $('[data-prod-' + prod_id + '-cur-price]').data('prod-' + prod_id + '-price-over-500');
    } else {
        cur_price = $('[data-prod-' + prod_id + '-cur-price]').data('prod-' + prod_id + '-price');
    }
    $('[data-prod-' + prod_id + '-cur-price]').text(cur_price);
}

$(document).ready(function() {
    // липкий блок "Проверьте заказ"
    if ( $(window).width() >= 768 ) {
        var $offsetTop = parseFloat( $('.header').outerHeight() ) + parseFloat( $('.header').css('margin-bottom') ) + 20;
        $('.block_check-order').stick_in_parent({
            offset_top: $offsetTop
        });
    }

    // счётчик товара
    var timeout;
    $(document).on('click', '[data-count-change]', function(e) {
        e.preventDefault();

        var count_field = $(this).siblings('[name="count"]'),
            count_field_val = count_field.val(),
            max_count = count_field.data('max-count'),
            prod_id = Number(count_field.attr('id').replace(/\D+/g,""));

        clearTimeout(timeout);

        if (
            ( parseInt($(this).data('count-change')) == 1  && parseInt( count_field.val() ) >= 1 && parseInt( count_field.val() ) < max_count ) ||
            ( parseInt($(this).data('count-change')) == -1 && parseInt( count_field.val() ) > 1 && parseInt( count_field.val() ) <= max_count )
        ) {
            count_field.val( count_field_val.replace(/^(\d+)/, parseInt(count_field.val()) + parseInt($(this).data('count-change')) ) );
            rewriteProductPrice(count_field, prod_id);
        }

        // добавление товара в корзину при изменении количества
        if ( $('.cart-page').length ) {
            timeout = setTimeout(function() {
                count_field.trigger('change');
            }, 1000);
        }
    });

    // запрет ввода не цифр и изменение цены товара в зависимости от выбранного кол-ва
    $(document).on('input', '[name="count"]', function() {
        var val = $(this).val(),
            input = $(this),
            max_count = input.data('max-count'),
            prod_id = Number(input.attr('id').replace(/\D+/g,""));

        (parseInt(val) == 0) ? $(this).val(val.replace(/\D/g, 1)) : $(this).val(val.replace(/\D/g, ''));

        clearTimeout(timeout);

        // не даём ввести число больше остатка товара в наличии
        if ( parseInt(val) > parseInt(max_count) )
            $(this).val(max_count);

        rewriteProductPrice(input, prod_id);
    });

    $('input[name="count"]').focus(function() {
        $(this).val( parseInt($(this).val()) );
    });

    $('input[name="count"]').blur(function() {
        $(this).val( parseInt( $(this).val() ) + ' шт' );

        timeout = setTimeout(function() {
            if ( $('.cart-page').length ) {
                $(this).trigger('change');
            }
        }, 1000);
    });

    // если при потере фокуса с поля ввода количества товара это поле пустое то записываем в него 1
    $('[name="count"]').blur(function() {
        if ($(this).val() == '')
            $(this).val(1);
    });
    // \\ счётчик товара


    // меняем количество товаров для вывода
    $(document).on('change', 'SELECT[name="products_in_table"]', function(event) {
        event.preventDefault();

        var key = $(this).attr('name'),
            value = $(this).find('OPTION:selected').val();

        $.ajax({
            type: 'POST',
            data: {
                action: 'setSession',
                params: {
                    key: key,
                    value: value
                }
            },
            success: function(res) {
                window.location.reload();
            }
        });
    });


    // переключение вкладок на странице продукта
    $(document).on('change', '[name="subcategory_page_tabs_label"]', function(event) {
        event.preventDefault();

        var tab_name = $(this).val();

        if ( $(document).width() >= 768 ) {
            $('[name="subcategory_page_tabs_label"]').parent('LABEL').removeClass('active');
            $(this).parent('LABEL').addClass('active');

            $('[id^="subcategory_page_tab_"]:not(.d-none)').addClass('d-none');
            $('[id="subcategory_page_tab_' + tab_name + '"]').removeClass('d-none');
        } else {
            if ( !$(this).parent('LABEL').hasClass('active') )
                $(this).parent('LABEL').addClass('active');
            else
                $(this).parent('LABEL').removeClass('active');

            if ( !$('[id="subcategory_page_tab_' + tab_name + '"]').hasClass('d-none') )
                $('[id="subcategory_page_tab_' + tab_name + '"]').addClass('d-none');
            else
                $('[id="subcategory_page_tab_' + tab_name + '"]').removeClass('d-none');
        }
    });
    // \\ переключение вкладок на странице продукта


    // скрытие/отображение пунктов самовывоза при смене способа доставки на странице оформления заказа
    $(document).on('change', '[name="delivery"]', function(e) {
        if ( $(this).val() != 1 ) { // 1 - самовывоз 2 - курьером 3 - транспортной компанией
            $('#pickup_points_list').addClass('d-none');
            $('#address-for-delivery').removeClass('d-none');
            $('[name="extfld_pickup_point"]:checked').removeAttr('checked');
        }

        if ( $(this).val() == 1 ) {
            $('#pickup_points_list').removeClass('d-none');
            $('#address-for-delivery').addClass('d-none');
            $('[name="extfld_pickup_point"]:first').attr('checked', 'checked');
        }

        if ( $(this).val() == 3 ) {
            $('#transport_companies').removeClass('d-none');
        } else {
            $('#transport_companies').addClass('d-none');
        }
    })
    // \\ скрытие/отображение пунктов самовывоза при смене способа доставки на странице оформления заказа
});