/* безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */
$(document).on('click', '[target="_blank"]', function(e) {e.preventDefault();var otherWindow = window.open();otherWindow.opener = null;otherWindow.location = $(this).attr('href');});


// разобраться почему не отрабатывает
function setHeaderBottomRowHeight() {
    if ( $('HTML[data-device="mobile"]').length == 0 )
        return false;
    
    var window_height = $(window).height(),
        header_top_row_height_vh = 100 - ( $('.header__row_top').outerHeight() / window_height ) * 100;

    $('#header_bottom_row').css('height', header_top_row_height_vh + 'vh').css('max-height', header_top_row_height_vh + 'vh');
}

$(window).resize(function() {
    setHeaderBottomRowHeight();
});

$(document).ready(function() {
    setHeaderBottomRowHeight();

    // скрываем прелоадер
    $('#preloader').delay(200).fadeOut(500);

    // стилизуем select'ы
    $('SELECT').styler();

    // добавляем верхний отступ для контента, чтобы он не заходил под шапку сайта
    $('HTML[data-device!="desktop"] BODY > main, HTML[data-device="desktop"] BODY:not(.mainpage) > main').css('padding-top', $('.header').outerHeight() + 'px');

    //меню каталога (разбить содержимое на колонки в зависимости от количества пунктов меню первого уровня)
    (function separateSubmenuItem() {
        var menuItemsLvl1 = $('[data-cat-nav]').find('.lvl-1');
        var countMenuItemsLvl1 = menuItemsLvl1.length;

        menuItemsLvl1.each(function() {
            var menuItemsLvl2 = $(this).find('.lvl-2');
            var countMenuItemsLvl2 = menuItemsLvl2.length;

            if(countMenuItemsLvl2 >= countMenuItemsLvl1) {
                $(this).find('.cat__submenu').css('column-count', '2');
            };
        });
    })();

    //инициализация маски для телефона
    $('input[name="phone"]').mask('+7 (999) 999-99-99');

    //инициализация popup
    $.extend(true, $.magnificPopup.defaults, {
        tClose: 'Закрыть (Esc)', // Alt text on close button
        tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
        gallery: {
            tPrev: 'Предыдущий (Стрелка влево)', // Alt text on left arrow
            tNext: 'Следующий (Стрелка вправо)', // Alt text on right arrow
            tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
        },
        image: {
            tError: '<a href="%url%">Изображение</a> не может быть загружено.' // Error message when image could not be loaded
        },
        ajax: {
            tError: '<a href="%url%">Содержимое</a> не может быть загружено.' // Error message when ajax request failed
        },

        callbacks: {
            open: function() {
                if ( $(window).width() >= 768 )
                    $('.wrapper').css('margin-right', '-16px');
            },
            close: function() {
                if ( $(window).width() >= 768 )
                    $('.wrapper').css('margin-right', 0);

                $($.magnificPopup.instance.items[0].src + ' .input.error').removeClass('error');
            }
        }
    });

    $('[data-popup]').each(function() {
        var self = $(this),
            popup_type = self.data('popup') ? self.data('popup') : 'inline';
                
        if ( popup_type == 'gallery' ) {
            self.magnificPopup({
                delegate: '.tile.swiper-slide:not(.swiper-slide-duplicate) a, *:not(.swiper-slide) > a',
                type: 'image',
                tLoading: 'Загрузка изображения #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
                },
                callbacks: {
                    buildControls: function() {
                        // re-appends controls inside the main container
                        this.arrowLeft.appendTo(this.contentContainer);
                        this.arrowRight.appendTo(this.contentContainer);
                    }
                }
            });
        } else {
            self.magnificPopup({
                type: popup_type,
                closeBtnInside: true,
                callbacks: {
                    beforeClose: function() {
                        $('.popup__error-msg, .popup__success-msg').text('');
                        $('.mfp-content form').trigger('reset');
                    }
                }
            });
        }
    });

    // сворачивание/разворачивание блоков
    $(document).on('click', '[data-toggle]', function(e) {
        e.preventDefault();

        if ( $(this).data('toggled-class') )
            var toggled_class = $(this).data('toggled-class');
        else
            var toggled_class = 'd-none';

        if ( $(this).data('close-other') != 0 ) {
            $('[data-toggle!="' + $(this).data('toggle') + '"]').each(function() {
                $( $(this).data('toggle') + ':not(.' + toggled_class + '):not(#header_menu)' ).addClass( toggled_class );
            });
            $('[data-toggle!="' + $(this).data('toggle') + '"].active').removeClass('active');
        }

        $( $(this).data('toggle') ).toggleClass(toggled_class);
        
        if ( $(this).data('remove-activator') ) {
            $(this).remove();
        } else {
            if ( $(this).data('active-btn-text') && !$(this).hasClass('active') ) {
                $(this).attr( 'data-default-btn-text', $(this).text() );
                $(this).text( $(this).data('active-btn-text') );
            } else if ( $(this).data('default-btn-text') && $(this).hasClass('active') ) {
                $(this).text( $(this).attr('data-default-btn-text') );
                $(this).removeAttr('data-default-btn-text');
            }

            $(this).toggleClass('active');

            // возвращаем меню в шапке в исходное состояние
            if ( $(this).data('toggle') == '#header_bottom_row' && $(this).hasClass('active') ) {
                // возвращаем исходные тексты кнопок
                $('#header_bottom_row [data-default-btn-text]').each(function() {
                    $(this).text( $(this).data('default-btn-text') );
                });

                // оставляем видимым только основное меню сайта
                $('#header_catalog_menu').addClass('d-none');
                $('#header_menu').removeClass('d-none');
            }
        }
    });

    //анимация input label в формах
    (function animateFormPlaceholders() {
        $('[data-form-input]').focusin(function(){
            $(this).siblings( $('.form__input-label') ).addClass('small');
        });

        $('[data-form-input]').focusout(function(){
            var val = $(this).val();

            if( val == '' || (val.replace( /[^\d+?()]/g, '')) == '+7()') {
                $(this).siblings( $('.form__input-label') ).removeClass('small');
            }
        });
    })();

    //скролл от футера к шапке сайта
    (function scrollToHeader() {
        $(document).on('click', '#btn-up', function(e) {
            e.preventDefault();
            $('html,body').animate({scrollTop:0 + "px"},{duration: 1E3});
        });
    })();

    //функционал для мобилки
    if( $(window).width() < 767 ) {
        // мобильное меню
        $(document).on('click', '#btn_menu_mobile', function(e) {
            e.preventDefault;

            $(this).toggleClass('close');
            $('.header__nav-wrap').fadeToggle();

            if( $('.header__btn-menu').hasClass('close')) {
                $('html,body').css('overflow', 'hidden');
                $('html,body').css('height', '110vh');
            } else{
                $('html,body').css('overflow', 'auto');
                $('html,body').css('height', 'auto');
            }
        });

        //обертка для таблиц для мобильного телефона
        $('table').wrap("<div class='table-wrap'></div>");
    }

    // ajax запросы при нажатии на кнопки с [data-ajax]
    $(document).on('click', '[data-ajax]', function(e) {
        e.preventDefault();

        var self = $(this),
            action = $(this).data('ajax'),
            params = ( $(this).data('ajax-params') ) ? $(this).data('ajax-params') : {},
            reload = self.data('ajax-reload');

        if ( self.parentsUntil('form').parent().length > 0 )
            var form = self.parentsUntil('form').parent();

        if ( form_data = form.serializeArray() )
            form_data.forEach (function(item) {
                params[item.name] = item.value;
            });

        $.ajax({
            type: 'POST',
            data: {
                action: action,
                params: params
            },
            success: function(res) { //
                res = JSON.parse(res);

                form.find('.form__input.success, .form__input.error').removeClass('success error');

                var message_timeout = (res.output) ? parseInt(res.output.replace(/<[^>]+>/g,'').length) * 75 : 2500, // задержка в мс зависящая от длины отображаемого сообщения
                    popup = self.parentsUntil('.popup').parent('.popup');

                if ( res.success ) {
                    switch ( action ) {
                        case 'auth':
                        case 'logout':
                            $('#header_profile_wrap').html(res.output);
                            if ( res.redirectTo )
                                window.location = res.redirectTo;
                        break;

                        case 'saveProfile':
                            $('#header_profile_wrap').html(res.header_profile);

                            form.find('.form__message').removeClass('success error').addClass('success').html(res.output);

                            setTimeout(function() {
                                form.find('.form__message').text('').removeClass('success error');
                            }, message_timeout);
                        break;

                        case 'repeatOrder':
                            $.jGrowl("Товары заказа №" + res.order_num + " добавлены в корзину", { life:4000, theme: 'af-message-success' });
                            $('[data-cart-count]').text( res.cart_count );
                            $('[data-cart-cost]').text( res.cart_cost );
                        break;
                    }

                    $('.popup__error-msg').text('');
                    if ( ['reg', 'resetPass'].indexOf(action) == -1 ) { // закрываем попап если это не регистрация и не сброс пароля
                        $.magnificPopup.close();
                    } else {
                        $('.popup__success-msg').text(res.output);
                    }

                    form.find('[type="password"]').each(function() {
                        $(this).val('');
                    });
                } else if ( popup && self.parentsUntil('.popup').parent('.popup').length > 0 ) {
                    popup.find('.popup__error-msg').html(res.output);
                } else if ( res.wrong_element_selector ) {
                    if ( !$(res.wrong_element_selector).hasClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error') )
                        $(res.wrong_element_selector).addClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error');

                    form.find('.form__message').removeClass('success error').addClass('error').text(res.output);
                    setTimeout(function() {
                        if ( action == 'saveProfile' && !res.success && res.redirectTo )
                            window.location = res.redirectTo;
                        else
                            form.find('.form__message').text('').removeClass('success error');
                    }, message_timeout);
                }

                if ( res.reload || (res.success && reload) )
                    window.location.reload();
            }
        });
    });
    // \\ ajax запросы при нажатии на кнопки с [data-ajax]

    // переключение вкладок в окне авторизации/регистрации
    $(document).on('change', '[name="auth_reg_popup_action"]', function() {
        var target_form = $( $(this).val() );

        $('.popup__error-msg, .popup__success-msg').text('');

        target_form.removeClass('d-none').siblings('form').addClass('d-none');
    });
    // \\ переключение вкладок в окне авторизации/регистрации
});

/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success) {
        $.magnificPopup.close();

        var inputFileLabel = $(this).find('[data-input-file]');
        inputFileLabel.text(inputFileLabel.data('input-file'));
        //удаляем все созданные input в форме
        $('[data-dropzone]').find('input[data-file-name]').remove();
        $('.dropzone__file-label').remove();
    }
});
/* \\ действия по успешной отправке формы через AjaxForm */

/* YANDEX MAPS. Ищет на странице элемент с атрибутом data-map="*адреса разделённые ||*" и вставляет в него карту с маркерами на указанных адресах */
(function maps() {

    var yandexMapsInit = function (c) {

        if (!$('[data-map').length) return false;


        $.getScript('https://api-maps.yandex.ru/2.1?apikey=' + $yandex_maps_api_key + '&lang=ru_RU', function () {

            ymaps.ready(init);

            function init() {
                $('[data-map]').each(function () {
                    var $t = $(this),
                        address = $t.data('map');

                    $t.html('');

                    ymaps.geocode(address, {
                        results: 1
                    }).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates();


                        var placemark = new ymaps.Placemark(coords, {
                            balloonContent: firstGeoObject.properties._data.balloonContent
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/assets/images/map_marker.svg',
                            iconImageSize: [160, 112],
                            iconImageOffset: [-80, -120],
                        });


                        var myMap = new ymaps.Map($t[0], {
                            center: coords,
                            zoom: 16,
                            controls: ['zoomControl', 'typeSelector']
                        });
                        myMap.behaviors.disable("scrollZoom");
                        myMap.geoObjects.add(placemark);
                    });
                });
            }
        });
    }


    $('[data-map]').each(function () {
        yandexMapsInit($(this));
    })
})();
/* \\ YANDEX MAPS */
